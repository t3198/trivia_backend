package com.aspari.trivia.controller;

import com.aspari.trivia.client.ClientQuestion;
import com.aspari.trivia.client.ClientQuestionMapper;
import com.aspari.trivia.repository.QuestionRepositoryImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/questions")
public class QuestionsController {

    QuestionRepositoryImpl repository = new QuestionRepositoryImpl();
    List<ClientQuestion> currentQuestions;

    @GetMapping("/{amount}")
    public List<ClientQuestion> retrieveQuestions(@PathVariable("amount") int amount) {
        currentQuestions.addAll(new ClientQuestionMapper().toDomainList(repository.getQuestions(amount)));
        return currentQuestions;
    }
}
