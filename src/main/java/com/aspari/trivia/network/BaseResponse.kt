package com.aspari.trivia.network

data class BaseResponse<T>(val responseCode: Int, val results: List<T>)
