package com.aspari.trivia.dto;

public interface IDomainMapper<DTO, DomainModel> {
    DomainModel mapToDomainModel(DTO dto);

    DTO mapFromDomainModel(DomainModel domainModel);
}
