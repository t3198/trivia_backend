package com.aspari.trivia.dto

import com.aspari.trivia.network.BaseResponse

/*{
  "category": "Entertainment: Video Games",
  "type": "multiple",
  "difficulty": "hard",
  "question": "In &quot;Super Mario Sunshine&quot;, how do you unlock the &quot;Corona Mountain&quot; level?",
  "correct_answer": "By clearing every 7th episode",
  "incorrect_answers": [
    "By obtaining 70 Shines",
    "By clearing every episode involving a &quot;Secret&quot;",
    "By unlocking every nozzle F.L.U.D.D can use"
  ]
}*/
data class QuestionDto (
    val category: String,
    val type: String,
    val difficulty: String,
    val question: String,
    val correct_answer: String,
    val incorrect_answers: List<String>
)
