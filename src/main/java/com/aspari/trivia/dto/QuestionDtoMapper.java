package com.aspari.trivia.dto;

import com.aspari.trivia.domain.Answer;
import com.aspari.trivia.domain.Question;

import java.util.ArrayList;
import java.util.List;

public class QuestionDtoMapper implements IDomainMapper<QuestionDto, Question> {
    @Override
    public Question mapToDomainModel(QuestionDto questionDto) {
        Answer correctAnswer = new Answer(questionDto.getCorrect_answer(), true);
        List<Answer> incorrectAnswers = new ArrayList<>();
        questionDto.getIncorrect_answers().forEach(answer -> incorrectAnswers.add(new Answer(answer, false)));
        return new Question(
                questionDto.getCategory(),
                questionDto.getType(),
                questionDto.getDifficulty(),
                questionDto.getQuestion(),
                correctAnswer,
                incorrectAnswers
        );
    }

    @Override
    public QuestionDto mapFromDomainModel(Question question) {
        List<String> incorrectAnswers = new ArrayList<>();
        question.getIncorrectAnswers().forEach(answer -> incorrectAnswers.add(answer.getAnswer()));

        return new QuestionDto(
                question.getCategory(),
                question.getType(),
                question.getDifficulty(),
                question.getQuestion(),
                question.getCorrectAnswer().getAnswer(),
                incorrectAnswers
        );
    }

    public List<Question> toDomainList(List<QuestionDto> dtoList) {
        List<Question> domainList = new ArrayList<>();
        dtoList.forEach(item -> domainList.add(mapToDomainModel(item)));
        return domainList;
    }

}
