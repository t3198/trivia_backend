package com.aspari.trivia.domain

import java.util.*

/*
API response https://opentdb.com/api.php?amount=5
{
  "category": "Entertainment: Video Games",
  "type": "multiple",
  "difficulty": "hard",
  "question": "In &quot;Super Mario Sunshine&quot;, how do you unlock the &quot;Corona Mountain&quot; level?",
  "correct_answer": "By clearing every 7th episode",
  "incorrect_answers": [
    "By obtaining 70 Shines",
    "By clearing every episode involving a &quot;Secret&quot;",
    "By unlocking every nozzle F.L.U.D.D can use"
  ]
}
*/

data class Question @JvmOverloads constructor(
    val id: UUID = UUID.randomUUID(),
    val category: String,
    val type: String,
    val difficulty: String,
    val question: String,
    val correctAnswer: Answer,
    val incorrectAnswers: List<Answer>
)
