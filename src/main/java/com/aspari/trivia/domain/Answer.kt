package com.aspari.trivia.domain

import java.util.*

data class Answer @JvmOverloads constructor(
    val id: UUID = UUID.randomUUID(),
    val answer: String,
    val correct: Boolean
)
