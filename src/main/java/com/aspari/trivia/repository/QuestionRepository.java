package com.aspari.trivia.repository;

import com.aspari.trivia.domain.Question;

import java.util.List;

public interface QuestionRepository {
    List<Question> getQuestions(int amount);
}
