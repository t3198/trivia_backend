package com.aspari.trivia.repository;

import com.aspari.trivia.domain.Question;
import com.aspari.trivia.dto.QuestionDto;
import com.aspari.trivia.dto.QuestionDtoMapper;
import com.aspari.trivia.network.BaseResponse;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.Reader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class QuestionRepositoryImpl implements QuestionRepository {

    @Override
    public List<Question> getQuestions(int amount) {
        final Duration TIMEOUT = Duration.ofSeconds(5);
        final String uri = "https://opentdb.com";
        final String param = String.format("/api.php?amount=%s", amount);
        BaseResponse<QuestionDto> result = WebClient.create(uri)
                .get()
                .uri(param)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<BaseResponse<QuestionDto>>() {
                }).block(TIMEOUT);
        return result != null ? new QuestionDtoMapper().toDomainList(result.getResults()) : new ArrayList<>();
    }
}
