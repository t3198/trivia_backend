package com.aspari.trivia.client;

import com.aspari.trivia.domain.Answer;
import com.aspari.trivia.domain.Question;
import com.aspari.trivia.dto.IDomainMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ClientQuestionMapper implements IDomainMapper<Question, ClientQuestion> {
    @Override
    public ClientQuestion mapToDomainModel(Question question) {
        List<ClientAnswer> answers = question.getIncorrectAnswers()
                .stream()
                .map(answer -> new ClientAnswer(answer.getId(), answer.getAnswer()))
                .collect(Collectors.toList());
        answers.add(new ClientAnswer(question.getCorrectAnswer().getId(), question.getCorrectAnswer().getAnswer()));
        return new ClientQuestion(
                question.getCategory(),
                question.getType(),
                question.getDifficulty(),
                question.getQuestion(),
                answers
        );
    }

    @Override
    public Question mapFromDomainModel(ClientQuestion clientQuestion) {
        return null;
    }

    public List<ClientQuestion> toDomainList(List<Question> questionList) {
        List<ClientQuestion> domainList = new ArrayList<>();
        questionList.forEach(item -> domainList.add(mapToDomainModel(item)));
        return domainList;
    }
}
