package com.aspari.trivia.client

import java.util.*

data class ClientQuestion @JvmOverloads constructor(
    val id: UUID = UUID.randomUUID(),
    val category: String,
    val type: String,
    val difficulty: String,
    val question: String,
    val answers: List<ClientAnswer>
)
