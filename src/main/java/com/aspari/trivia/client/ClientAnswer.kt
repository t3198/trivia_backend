package com.aspari.trivia.client

import java.util.*

data class ClientAnswer(
    val id: UUID,
    val answer: String
)
